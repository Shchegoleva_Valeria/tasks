package Task1;

import java.util.Scanner;

/**
 * Напишите программу, которая считывает символы пока не встретится точка.
 * Также предусмотрите вывод количества пробелов.
 *
 * @author Щеголева В., группа 15/18
 */
public class Point {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите строку: ");
        String symbol = scan.nextLine();
        int i = symbol.indexOf('.');
        int space = 0;
        char[] arrayChar = symbol.toCharArray();
        for (int j = 0; j < arrayChar.length; j++) {
            if (arrayChar[j] == ' ') {
                space++;
            }
        }
        System.out.println("Количество символов: " + i + ". Количество пробелов: " + space);
    }
}

