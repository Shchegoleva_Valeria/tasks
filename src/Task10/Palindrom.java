package Task10;

import java.util.Scanner;

/**
 * Напишите метод, который будет проверять является ли число палиндромом
 * (одинаково читающееся в обоих направлениях).
 *
 * @author Щеголева В., группа 15/18
 */

public class Palindrom {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите число: ");
        int num = scan.nextInt();

        String text = Integer.toString(num);

        if (check(text)==true){
            System.out.println("Палиндром");
        }else {
            System.out.println("Не палиндром");
        }
    }

    private static boolean check (String convert) {
        return convert.equals((new StringBuilder(convert)).reverse().toString());
    }
}
