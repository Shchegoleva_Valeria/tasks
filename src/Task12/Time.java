package Task12;

import java.util.Scanner;

/**
 * Напишите программу, которая будет считать количество часов, минут и секунд в n-ном количестве суток.
 *
 * @author Щеголева В., группа 15/18
 */
public class Time {
    static Scanner scan = new Scanner(System.in);

    public static void main (String [] args){
        System.out.println("Введите количество дней: ");
        int day = scan.nextInt();
        int hours = day * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println("В " + day + " сут. " + hours + " часов");
        System.out.println("В " + day + " сут. " + minutes + " минут");
        System.out.println("В " + day + " сут. " + seconds + " секунд");
    }
}
