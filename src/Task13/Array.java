package Task13;

import java.util.Random;
import java.util.Scanner;

/**
 * Из двумерного массива заполненного случайными числами
 * перенесите построчно эти числа в одномерный массив.
 *
 * @author Щеголева В., группа 15/18
 */
public class Array {
    static Scanner scan = new Scanner(System.in);
    static Random rm = new Random();

    public static void main(String[] args) {
        int[][] massiv = new int[scan.nextInt()][scan.nextInt()];
        int[] array = new int[massiv.length * massiv[1].length];
        fill(massiv);
        print(massiv);
        transfer(massiv, array);
        System.out.println("Полученная строка: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");

        }
    }

    private static void transfer(int[][] massiv, int[] array) {
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                array[i * massiv[i].length + j] = massiv[i][j];
            }
        }
    }


    private static void fill(int[][] massiv) {
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                massiv[i][j] = rm.nextInt(30);
            }


        }
    }

    private static void print (int[][] massiv){
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                System.out.print(massiv[i][j] + " ");
            }
            System.out.println();
        }
    }
}
