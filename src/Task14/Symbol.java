package Task14;

import java.util.Scanner;

/**
 * Определить является ли символ введенный с клавиатуры цифрой, буквой или знаком пунктуации.
 *
 * @author Щеголева В., группа 15/18.
 */

public class Symbol {
    static Scanner scan = new Scanner(System.in);

    public static void main (String [] args){
        System.out.println("Введите символ: ");
        String str = scan.next();
        char num = str.charAt(0);
        if (Character.isDigit(num)) {
            System.out.print("Цифра.");
        }
        if (Character.isLetter(num)) {
            System.out.print("Буква.");
        }
        if ("!;:?,.".contains(str)) {
            System.out.print("Пунктуационный знак.");
        }
    }
}
