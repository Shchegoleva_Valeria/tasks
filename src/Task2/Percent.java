package Task2;

import java.util.Scanner;

/**
 * Напишите метод, который будет увеличивать заданный элемент массива на 10%.
 *
 * @author Щеголева В., группа 15/18
 */
public class Percent {
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество элементов в массиве: ");
        int n = scan.nextInt();
        double [] array = new double [n];
        System.out.println("Введите номер элемента: ");
        int a = scan.nextInt()-1;

        for (int i = 0; i <array.length; i++) {
            array[i] = 1 + (int) (Math.random()*(100));
            System.out.print(array[i] + " ");
        }

        array[a] = array[a] + array[a]/100*10;
        System.out.println();
        System.out.println(array[a]);
    }
}
