package Task3;

import java.util.Scanner;

/**
 *Напишите метод для перевода рублей в евро по заданному курсу.
 * В качестве аргументов передайте количество рублей и курс.
 *
 * @author Щеголева В., группа 15/18
 */
public class Euro {
    static Scanner scan = new Scanner (System.in);

    public static void conversion(double course, double ruble){
        double euro = ruble/course;
        System.out.println(euro);
    }

    public static void main (String [] args) {
        System.out.println("Введите курс евро: ");
        double course = scan.nextDouble();
        System.out.println("Введите количество рублей: ");
        double ruble = scan.nextDouble();
        conversion(course, ruble);

    }
}