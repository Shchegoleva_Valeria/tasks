package Task4;

import java.util.Scanner;

/**
 *Расчет расстояния до места удара молнии.
 * Звук в воздухе распространяется со скоростью приблизительно равной 1234,8 километров в час.
 * Зная интервал времени между вспышкой молнии и звуком сопровождающим ее можно рассчитать расстояние.
 * Допустим интервал 6,8 секунды.
 *
 * @author Щеголева В., группа 15/18
 */

public class Lightning {
    static Scanner scan = new Scanner(System.in);
    public static void main (String [] args) {
        double speed;
        double time;

        System.out.println("Введите скорость звука: ");
        speed = scan.nextDouble();
        System.out.println("Введите интервал времени между вспышкой молнии и звуком, сопровождающим ее: ");
        time = scan.nextDouble();

        distance(speed, time);

    }

    public static void distance (double speed, double time) {
        double distance = speed * time;
        System.out.println(distance);
    }
}
