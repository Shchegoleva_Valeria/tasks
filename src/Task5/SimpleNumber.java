package Task5;

/**
 *Напишите программу, которая вычислит простые числа в пределах от 2 до 100.
 *
 * @author Щеголева В., группа 15/18
 */
public class SimpleNumber {
    public static void number() {

        for (int i = 2; i < 100; i++) {
            if (i % 2 == 0 && i != 2 || i % 3 == 0 && i != 3 || i % 5 == 0 && i != 5 || i % 7 == 0 && i != 7) {
                System.out.print("");
            } else {
                System.out.print(i + " ");
            }

        }
    } public static void main (String [] args){
        number();
    }
}
