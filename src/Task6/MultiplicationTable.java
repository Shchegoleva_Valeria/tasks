package Task6;

import java.util.Scanner;

/**
 *Напишите программу, которая будет выводить таблицу умножения введенного пользователем числа с клавиатуры.
 *
 * @author Щеголева В., группа 15/18
 */
public class MultiplicationTable {
    static Scanner scan = new Scanner(System.in);
    public static void multiplication(int number, int n){
        for (int i = 1; i <= n; i++){
            int num = number * i;
            System.out.println(number + " * " + i + " = " + num);
        }
    }
    public static void main (String [] args) {
        System.out.println("Введите число: ");
        int number = scan.nextInt();
        System.out.println("Введите количество множителей: ");
        int n = scan.nextInt();
        multiplication(number, n);
    }
}
