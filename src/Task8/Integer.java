package Task8;

import java.util.Scanner;

/**
 *Напишите программу, которая будет проверять является ли число типа double целым.
 *
 *@author Щеголева В., группа 15/18
 */

public class Integer {
    static Scanner scan = new Scanner(System.in);
    public static void main (String [] args){
        System.out.println("Введите число: ");
        double n = scan.nextDouble();
        integer(n);
    }

    public static void integer(double n){
        if(n%1==0){
            System.out.println("Число целое");
        }
        else{
            System.out.println("Число дробное");
        }
    }

}
