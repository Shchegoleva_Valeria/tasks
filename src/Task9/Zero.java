package Task9;

import java.util.Scanner;

/**
 * Создайте метод, который в качестве аргумента получает число и полностью обнуляет столбец прямоугольной матрицы,
 * который соответствует заданному числу.
 *
 * @author Щеголева В., группа 15/18
 */
public class Zero {
    static Scanner scan = new Scanner(System.in);

    public static void main(String [] args){
        int [] [] arr = {
                {3, 3, 5, 4},
                {5, 6, 2, 8},
                {1, 4, 7, 9},
                {3, 5, 8, 2},
                {7, 1, 7, 0}
        };
        System.out.println("Введите номер столбца: ");
        int n = scan.nextInt();
        zero(arr, n);
        print(arr);

    }

    public static void zero(int arr[][], int n){
        for (int i = 0; i < arr.length; i++) {
            arr[i][n] = 0;
        }

    }

    public static void print(int arr [][]){
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }
}